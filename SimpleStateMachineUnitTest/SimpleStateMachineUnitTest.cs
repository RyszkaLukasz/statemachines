﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utility.SimpleStateMachine;

namespace SimpleStateMachineUnitTest
{
    [TestClass]
    public class SimpleStateMachineUnitTest
    {
        class MethodStatus
        {
            public bool WasEnterExecuted { get; set; }
            public long EnterTicks { get; set;  }

            public bool WasExitExecuted { get; set; }
            public long ExitTicks { get; set; }
        }

        private StateMachine _stateMachine;
        private Dictionary<OpcStates, MethodStatus> _methodExecuted;

        enum OpcStates : int
        {
            Initialization = StateType.Init,
            Connected,
            Disconnected
        }

        [TestMethod]
        public void FlatSimpleStateMachineTest()
        {
            InitFlatSimpleStateMachine();

            Assert.IsFalse(_methodExecuted[OpcStates.Initialization].WasEnterExecuted);
            Assert.IsFalse(_methodExecuted[OpcStates.Initialization].WasExitExecuted);
            Assert.IsFalse(_methodExecuted[OpcStates.Connected].WasEnterExecuted);
            Assert.IsFalse(_methodExecuted[OpcStates.Connected].WasExitExecuted);
            Assert.IsFalse(_methodExecuted[OpcStates.Disconnected].WasEnterExecuted);
            Assert.IsFalse(_methodExecuted[OpcStates.Disconnected].WasExitExecuted);
            Assert.AreEqual(_stateMachine.CurrentState, (int)OpcStates.Initialization);

            _stateMachine.Init();
            Assert.IsTrue(_methodExecuted[OpcStates.Initialization].WasEnterExecuted);
            Assert.IsFalse(_methodExecuted[OpcStates.Initialization].WasExitExecuted);
            Assert.IsFalse(_methodExecuted[OpcStates.Connected].WasEnterExecuted);
            Assert.IsFalse(_methodExecuted[OpcStates.Connected].WasExitExecuted);
            Assert.IsFalse(_methodExecuted[OpcStates.Disconnected].WasEnterExecuted);
            Assert.IsFalse(_methodExecuted[OpcStates.Disconnected].WasExitExecuted);
            Assert.AreEqual(_stateMachine.CurrentState, (int)OpcStates.Initialization);

            _stateMachine.Transition((int)OpcStates.Connected);
            Assert.IsTrue(_methodExecuted[OpcStates.Initialization].WasEnterExecuted);
            Assert.IsTrue(_methodExecuted[OpcStates.Initialization].WasExitExecuted);
            Assert.IsTrue(_methodExecuted[OpcStates.Connected].WasEnterExecuted);
            Assert.IsFalse(_methodExecuted[OpcStates.Connected].WasExitExecuted);
            Assert.IsFalse(_methodExecuted[OpcStates.Disconnected].WasEnterExecuted);
            Assert.IsFalse(_methodExecuted[OpcStates.Disconnected].WasExitExecuted);
            Assert.IsTrue(_methodExecuted[OpcStates.Initialization].ExitTicks < _methodExecuted[OpcStates.Connected].EnterTicks);
            Assert.AreEqual(_stateMachine.CurrentState, (int)OpcStates.Connected);

            _stateMachine.Transition((int)OpcStates.Disconnected);
            Assert.IsTrue(_methodExecuted[OpcStates.Initialization].WasEnterExecuted);
            Assert.IsTrue(_methodExecuted[OpcStates.Initialization].WasExitExecuted);
            Assert.IsTrue(_methodExecuted[OpcStates.Connected].WasEnterExecuted);
            Assert.IsTrue(_methodExecuted[OpcStates.Connected].WasExitExecuted);
            Assert.IsTrue(_methodExecuted[OpcStates.Disconnected].WasEnterExecuted);
            Assert.IsFalse(_methodExecuted[OpcStates.Disconnected].WasExitExecuted);
            Assert.IsTrue(_methodExecuted[OpcStates.Connected].ExitTicks < _methodExecuted[OpcStates.Disconnected].EnterTicks);
            Assert.AreEqual(_stateMachine.CurrentState, (int)OpcStates.Disconnected);

            _stateMachine.Transition((int)OpcStates.Connected);
            Assert.IsTrue(_methodExecuted[OpcStates.Initialization].WasEnterExecuted);
            Assert.IsTrue(_methodExecuted[OpcStates.Initialization].WasExitExecuted);
            Assert.IsTrue(_methodExecuted[OpcStates.Connected].WasEnterExecuted);
            Assert.IsTrue(_methodExecuted[OpcStates.Connected].WasExitExecuted);
            Assert.IsTrue(_methodExecuted[OpcStates.Disconnected].WasEnterExecuted);
            Assert.IsTrue(_methodExecuted[OpcStates.Disconnected].WasExitExecuted);
            Assert.IsTrue(_methodExecuted[OpcStates.Disconnected].ExitTicks < _methodExecuted[OpcStates.Connected].EnterTicks);
            Assert.AreEqual(_stateMachine.CurrentState, (int)OpcStates.Connected);
        }

        private void InitFlatSimpleStateMachine()
        {
            _methodExecuted = new Dictionary<OpcStates, MethodStatus>()
            {
                {OpcStates.Initialization, new MethodStatus() { WasEnterExecuted = false, WasExitExecuted = false } },
                {OpcStates.Connected, new MethodStatus() { WasEnterExecuted = false, WasExitExecuted = false } },
                {OpcStates.Disconnected, new MethodStatus() { WasEnterExecuted = false, WasExitExecuted = false } },
            };

            State initializationState = new State(
                null,
                () =>
                {
                    _methodExecuted[OpcStates.Initialization].WasEnterExecuted = true;
                    _methodExecuted[OpcStates.Initialization].EnterTicks = DateTime.Now.Ticks;
                    Thread.Sleep(200);
                },
                () =>
                {
                    _methodExecuted[OpcStates.Initialization].WasExitExecuted = true;
                    _methodExecuted[OpcStates.Initialization].ExitTicks = DateTime.Now.Ticks;
                    Thread.Sleep(200);
                });
            State connectedState = new State(
                null,
                () =>
                {
                    _methodExecuted[OpcStates.Connected].WasEnterExecuted = true;
                    _methodExecuted[OpcStates.Connected].EnterTicks = DateTime.Now.Ticks;
                    Thread.Sleep(200);
                },
                () =>
                {
                    _methodExecuted[OpcStates.Connected].WasExitExecuted = true;
                    _methodExecuted[OpcStates.Connected].ExitTicks = DateTime.Now.Ticks;
                    Thread.Sleep(200);
                });
            State disconnectedState = new State(
                null,
                () =>
                {
                    _methodExecuted[OpcStates.Disconnected].WasEnterExecuted = true;
                    _methodExecuted[OpcStates.Disconnected].EnterTicks = DateTime.Now.Ticks;
                    Thread.Sleep(200);
                },
                () =>
                {
                    _methodExecuted[OpcStates.Disconnected].WasExitExecuted = true;
                    _methodExecuted[OpcStates.Disconnected].ExitTicks = DateTime.Now.Ticks;
                    Thread.Sleep(200);
                });

            _stateMachine = new StateMachine(new StateCollection()
            {
                { (int)OpcStates.Initialization, initializationState },
                { (int)OpcStates.Connected, connectedState },
                { (int)OpcStates.Disconnected, disconnectedState }
            });
        }
    }
}
