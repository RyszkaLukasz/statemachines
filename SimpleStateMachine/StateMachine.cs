﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utility.SimpleStateMachine
{
    public class StateMachine
    {
        private readonly StateCollection _states = new StateCollection();
        private readonly object _lock = new object();

        public StateMachine(StateCollection states)
        {
            _states = states;

            foreach (State state in _states.Values)
            {
                if (!(state.Parent is null) && state is State)
                {
                    state.Parent.Children.Add(state);
                }
            }

            foreach (State state in _states.Values)
            {
                if (state.Parent is null)
                {
                    int level = 0;
                    state.Level = level;
                    FillLevel(state, ref level);
                }
            }
        }

        public int CurrentState { get; protected set; } = (int)StateType.Init;

        public void Init()
        {
            CurrentState = (int) StateType.Init;
            _states[(int)StateType.Init]?.OnEnter();
        }

        private static void FillLevel(State state, ref int level)
        {
            level++;
            foreach (State child in state.Children)
            {
                child.Level = level;
                FillLevel(child, ref level);
            }
        }

        private static int GetCommonParentLevel(State stateA, State stateB)
        {
            if (stateA.Level > stateB.Level)
            {
                State nParent = stateA;
                for (int i = stateA.Level - stateB.Level; i >= 0; --i)
                {
                    nParent = nParent.Parent;
                }

                if (nParent.Parent is null && stateB.Parent is null)
                    return 0;

                if (stateB.Parent.Children.Contains(nParent))
                    return nParent.Level;
                else
                    return 0;
            }
            else
            {
                State nParent = stateB;
                for (int i = stateB.Level - stateA.Level; i > 0; --i)
                {
                    nParent = nParent.Parent;
                }

                if (nParent.Parent is null && stateA.Parent is null)
                    return 0;

                if (stateA.Parent.Children.Contains(nParent))
                    return nParent.Level;
                else
                    return 0;
            }
        }

        private static void CollectExitMethods(State state, ref List<Action> methods, int level)
        {
            if (!(state is null) && level <= state.Level)
            {
                if (!(state.OnExit is null))
                    methods.Add(state.OnExit);

                CollectExitMethods(state.Parent, ref methods, level);
            }
        }

        private static void CollectEnterMethods(State state, ref List<Action> methods, int level)
        {
            if (!(state is null) && level <= state.Level)
            {
                if (!(state.OnEnter is null))
                    methods.Add(state.OnEnter);

                CollectEnterMethods(state.Parent, ref methods, level);
            }
        }

        public void Transition(int type)
        {
            lock (_lock)
            {
                if (CurrentState == type)
                    return;

                if (!_states.ContainsKey(type))
                    throw new Exception("State type not found!");

                int commonParentLevel = GetCommonParentLevel(_states[CurrentState], _states[type]);

                List<Action> exitMethods = new List<Action>();
                List<Action> enterMethods = new List<Action>();

                CollectExitMethods(_states[CurrentState], ref exitMethods, commonParentLevel);
                CollectEnterMethods(_states[type], ref enterMethods, commonParentLevel);

                for (int i = 0; i < exitMethods.Count; ++i)
                {
                    exitMethods.ElementAt(i)();
                }

                for (int i = enterMethods.Count - 1; i >= 0; --i)
                {
                    enterMethods.ElementAt(i)();
                }

                CurrentState = type;
            }
        }
    }
}
