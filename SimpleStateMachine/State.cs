﻿using System;
using System.Collections.Generic;

namespace Utility.SimpleStateMachine
{
    public class State
    {
        public State(State parent, Action onEnter, Action onExit)
        {
            Parent = parent;
            OnEnter = onEnter;
            OnExit = onExit;
        }

        public State Parent { get; private set; } = null;

        public Action OnEnter { get; private set; } = null;

        public Action OnExit { get; private set; } = null;

        internal List<State> Children { get; set; } = new List<State>();
        internal int Level { get; set; } = -1;
    }
}
